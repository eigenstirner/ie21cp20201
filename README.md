# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| Gitlab User|
|---|---|
|Mateus Silva| [@eigenStirner](https://gitlab.com/eigenstirner)|
|| []()|
|| []()|
|| []()|

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  WIP

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
